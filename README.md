# DuckHunt Script

## Details

* language: TCL
* i8n: en & fr
* framework: eggdrop
* source: https://scripts.eggdrop.fr/details-Duck+Hunt-s228.html



## Current Notion

* Get it going somehow


## Future Plans

* Convert to `Limnoria` plugin combined with `shock`'s DuckBot and `oddlucks` plugin.



